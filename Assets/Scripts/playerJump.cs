﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerJump : MonoBehaviour {

    public GameObject text;
    
    public float gravity;
    public float force;

    private int m_score;

    float timer;

    private void Start()
    {
        text = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown("space"))
            timer = force;

        transform.Translate(new Vector3(0, timer, 0) * Time.deltaTime);

        if (timer > gravity)
            timer -= Time.deltaTime * 30;
        else
            timer = gravity;
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        switch(col.gameObject.tag)
        {
            case "Tube":
                Debug.Log("Looose");
                break;

            case "Score_trigger":
                {
                    m_score++;
                    text.GetComponent<viewerScript>().score = m_score;
                    Debug.Log("1 score!");
                }
                break;
        }
    }
}

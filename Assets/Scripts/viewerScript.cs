﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class viewerScript : MonoBehaviour {

    public GameObject Tube;
    public Text ScoreTxt;

    public int tubesNumber;
    public float timeBetweenTubes;

    public int score;

    GameObject[] Tubes;



    private void Start()
    {
        score = 0;
        Tubes = new GameObject[tubesNumber];
        StartCoroutine(spawnTubes());
    }

    private void Update()
    {
        ScoreTxt.text = score.ToString("0");

        if (Input.GetKey("escape"))
        {
            SceneManager.LoadScene("menu");
        }
    }

    IEnumerator spawnTubes()
    {
        for (int i = 0; i < tubesNumber; ++i)
        {
            Tubes[i] = Instantiate(Tube, new Vector3(19, Random.Range(-2.0f, 2.0f), 0), Quaternion.identity);
            yield return new WaitForSeconds(timeBetweenTubes);
        }

        while (true)
        {
            for (int i = 0; i < tubesNumber; ++i)
            {
                Tubes[i].transform.position = new Vector3(19, Random.Range(-2.0f, 2.0f), 0);
                yield return new WaitForSeconds(timeBetweenTubes);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tubeScript : MonoBehaviour {

    public float speed;

    private void Update()
    {
        transform.Translate(new Vector3(speed, 0, 0));
    }
}
